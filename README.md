# vault-approle-with-entities

Simple readme showing how to use AppRole with Vault Entities. You will need the Vault binary on your system and 2 terminal windows. 

### 1. Start a dev Vault server
In terminal 1, please start a dev Vault server.
```
vault server -dev
```

### 2. Create a secrets engine, application policy and 2 Auth methods
In terminal 2, issue the commands below. The root_token will be displayed just after you entered the `vault server -dev` command.
```
vault login <root_token>

# Create a secrets engine
vault secrets enable -path=testkv -version=1 kv
vault write testkv/postgres username=admin password=password

# Create an app policy
vault policy write pyapp - <<EOH
path "testkv/*" {
 capabilities = ["read", "list", "create", "update","delete"]
}
EOH

# Configure Approle Auth method and a role called my-role
vault auth enable approle
vault write auth/approle/role/my-role \
    secret_id_ttl=20m \
    token_num_uses=10 \
    token_ttl=30m \
    token_max_ttl=60m \
    secret_id_num_uses=40 \
    policies="pyapp"

# Save AppRole role ID and secret ID metadata
vault auth list -format=json | jq -r '.["approle/"].accessor' > approle_accessor.txt
vault read -format=json auth/approle/role/my-role/role-id > role.json
vault write -format=json -f auth/approle/role/my-role/secret-id > secretid.json

# Configure userpass Auth method
vault auth enable userpass
vault write auth/userpass/users/kawsar password=test
vault auth list -format=json | jq -r '.["userpass/"].accessor' > userpass_accessor.txt
```

### 3. Create an Entity and 2 entity Aliases
We will create one Entity Alias per Auth method
```
# Create an entity
cat <<EOF > payload.json
{  "name":"kawsar",
  "metadata": {
    "organization": "hashicorp",
    "team": "vault"
  },
  "policies": ["pyapp", "eng-dev", "infra-dev"] }
EOF

curl \
    --header "X-Vault-Token: $(vault print token)" \
    --request POST \
    --data @payload.json \
    http://127.0.0.1:8200/v1/identity/entity > entity.txt

# Create an entity alias with userpass
cat <<EOF > payload.json
{
  "name": "kawsar",
  "canonical_id": "$(cat entity.txt| jq -r .data.id)",
  "mount_accessor": "$(cat userpass_accessor.txt)"
}
EOF

curl \
    --header "X-Vault-Token: $(vault print token)" \
    --request POST \
    --data @payload.json \
    http://127.0.0.1:8200/v1/identity/entity-alias

# Create an entity alias with approle
cat <<EOF > payload.json
{
  "name": "$(cat role.json | jq -r .data.role_id)",
  "canonical_id": "$(cat entity.txt| jq -r .data.id)",
  "mount_accessor": "$(cat approle_accessor.txt)"
}
EOF

curl \
    --header "X-Vault-Token: $(vault print token)" \
    --request POST \
    --data @payload.json \
    http://127.0.0.1:8200/v1/identity/entity-alias

# Examine the 2 Aliases we setup
curl \
    --header "X-Vault-Token: $(vault print token)" \
    http://127.0.0.1:8200/v1/identity/entity/name/kawsar
```

### 4. Test AppRole and Userpass Logins
```
# Login with userpass, you should see "pyapp" role from entity
vault login -method=userpass username="kawsar" password="test"

# Login with AppRole, you should see "pyapp" role from entity
vault write auth/approle/login \
    role_id="$(cat role.json | jq -r .data.role_id )" \
    secret_id="$(cat secretid.json | jq -r .data.secret_id )"

vault token lookup
vault write testkv/postgres
```